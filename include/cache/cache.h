
//cache 是将键映射到值的接口。它具有内部同步，可以安全地从多个线程同时访问。
//它可能会自动驱逐条目以为新条目腾出空间。值对缓存容量具有指定的费用。例如，值是可变长度字符串的缓存可以使用字符串的长度作为字符串的费用。

// 提供了具有最近最少使用的驱逐策略的内置缓存实现。
// 如果客户想要更复杂的东西（如扫描抵抗、自定义驱逐策略、可变缓存大小等），他们可以使用他们自己的实现。

#ifndef MLDB_CACHE_H_
#define MLDB_CACHE_H_

#include <cstdint>

#include "include/slice.hpp"

namespace mldb {

class  Cache;

// Create a new cache with a fixed size capacity.  This implementation
// of cache uses a least-recently-used eviction policy.
Cache* NewLRUCache(size_t capacity);

class Cache {
 public:
  Cache() = default;

  Cache(const Cache&) = delete;
  Cache& operator=(const Cache&) = delete;

  // Destroys all existing entries by calling the "deleter"
  // function that was passed to the constructor.
  virtual ~Cache();

  // Opaque handle to an entry stored in the cache.
  struct Pair {
      Slice key;
      Slice val;
  };

  // 将 key->value 的映射插入到缓存中，并根据总缓存容量为其分配指定的费用。返回对应于映射的句柄。
  // 当不再需要返回的映射时，调用者必须调用 this->Release(handle)。当不再需要插入的条目时，键和值将传递给“删除器”。
  virtual void Insert(const Slice& key, void* value) = 0;

  // 如果缓存没有“key”的映射，则返回 nullptr。
  // 否则返回对应于映射的句柄。当不再需要返回的映射时，调用者必须调用 this->Release(handle)。
  virtual Pair* Lookup(const Slice& key) = 0;

  // 释放先前 Lookup() 返回的映射。要求：句柄必须尚未被释放。要求：句柄必须由对此的方法返回。
  virtual void Release(Pair* handle) = 0;

  // Return the value encapsulated in a handle returned by a
  // successful Lookup().
  // REQUIRES: handle must not have been released yet.
  // REQUIRES: handle must have been returned by a method on *this.
  virtual void* Value(Pair* handle) = 0;

  // If the cache contains entry for key, erase it.  Note that the
  // underlying entry will be kept around until all existing handles
  // to it have been released.
  virtual void Erase(const Slice& key) = 0;

  // Return a new numeric id.  May be used by multiple clients who are
  // sharing the same cache to partition the key space.  Typically the
  // client will allocate a new id at startup and prepend the id to
  // its cache keys.
  virtual uint64_t NewId() = 0;

  // Remove all cache entries that are not actively in use.  Memory-constrained
  // applications may wish to call this method to reduce memory usage.
  // Default implementation of Prune() does nothing.  Subclasses are strongly
  // encouraged to override the default implementation.  A future release of
  // leveldb may change Prune() to a pure abstract method.
  virtual void Prune() {}

  // Return an estimate of the combined charges of all elements stored in the
  // cache.
  virtual size_t TotalCharge() const = 0;
};

}  // namespace leveldb

#endif  // STORAGE_LEVELDB_INCLUDE_CACHE_H_
