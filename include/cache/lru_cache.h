#include <unordered_map>
#include "cache.h"

namespace mldb{
    template<typename T>
    class LRUCache :public Cache{
    public:
        LRUCache(int capacity) {
            head_=new Node();
            tail_=new Node();
            head_->next_=tail_;
            tail_->pre_=head_;
            capacity_=capacity;
        }

        T get(T key) {
            if(map_.find(key)!=map_.end()){
                Node *found=map_[key];
                MoveToTail(found);
                return found->val_;
            }else{
                return -1;
            }
        }

        void put(T key, T value) {
            if(map_.find(key)!=map_.end()){
                Node *found=map_[key];
                MoveToTail(found);
                found->val_=value;
            }else{
                if(size_==capacity_){
                    RemoveOldest();
                    size_--;
                }
                Node* new_node=new Node(key,value);
                map_[key]=new_node;
                AddToTail(new_node);
                size_+=key.size()+value.size();
            }
        }
    private:
        struct Node{
            Node()=default;
            Node(T key,T val):key_(key),val_(val){}
            T key_;
            T val_;
            Node* pre_=nullptr;
            Node* next_=nullptr;
        };
        std::unordered_map<T,Node*>map_;
        Node *head_=nullptr;
        Node *tail_=nullptr;
        uint64_t capacity_=1024*1024*1024;//1G
        uint64_t size_=0;
        void AddToTail(Node *new_node){
            Node *before_tail=tail_->pre_;
            before_tail->next_=new_node;
            tail_->pre_=new_node;
            new_node->pre_=before_tail;
            new_node->next_=tail_;
        }
        void RemoveOldest(){
            Node* del=head_->next_;
            head_->next_=del->next_;
            del->next_->pre_=head_;
            map_.erase(del->key_);
            delete(del);
        }
        void MoveToTail(Node* cur){
            cur->pre_->next_=cur->next_;
            cur->next_->pre_=cur->pre_;
            AddToTail(cur);
        }

    };



}
