#pragma once
#include <string>
#include <algorithm>
#include <cstdint>
#include <string>
#include <type_traits>
#include "include/slice.hpp"
#include "util/no_destructor.h"
namespace mldb {

class Slice;

// A Comparator object provides a total order across slices that are
// used as keys in an sstable or a database.  A Comparator implementation
// must be thread-safe since leveldb may invoke its methods concurrently
// from multiple threads.
class Comparator {
 public:
    ~Comparator()=default;

  // Three-way comparison.  Returns value:
  //   < 0 iff "a" < "b",
  //   == 0 iff "a" == "b",
  //   > 0 iff "a" > "b"
  virtual int Compare(const Slice& a, const Slice& b) const = 0;

  // The name of the comparator.  Used to check for comparator
  // mismatches (i.e., a DB created with one comparator is
  // accessed using a different comparator.
  //
  // The client of this package should switch to a new name whenever
  // the comparator implementation changes in a way that will cause
  // the relative ordering of any two keys to change.
  //
  // Names starting with "leveldb." are reserved and should not be used
  // by any clients of this package.
  virtual const char* Name() const = 0;

  // Advanced functions: these are used to reduce the space requirements
  // for internal data structures like index blocks.

  // If *start < limit, changes *start to a short string in [start,limit).
  // Simple comparator implementations may return with *start unchanged,
  // i.e., an implementation of this method that does nothing is correct.
  virtual void FindShortestSeparator(std::string* start,
                                     const Slice& limit) const = 0;

  // Changes *key to a short string >= *key.
  // Simple comparator implementations may return with *key unchanged,
  // i.e., an implementation of this method that does nothing is correct.
  virtual void FindShortSuccessor(std::string* key) const = 0;
};

// Return a builtin comparator that uses lexicographic byte-wise
// ordering.  The result remains the property of this module and
// must not be deleted.
  Comparator* BytewiseComparator();


    namespace {
        class BytewiseComparatorImpl : public Comparator {
        public:
            BytewiseComparatorImpl() = default;

            const char* Name() const override { return "leveldb.BytewiseComparator"; }

            int Compare(const Slice& a, const Slice& b) const override {
                return a.compare(b);
            }

            void FindShortestSeparator(std::string* start,
                                       const Slice& limit) const override {
                // Find length of common prefix
                size_t min_length = std::min(start->size(), limit.size());
                size_t diff_index = 0;
                while ((diff_index < min_length) &&
                       ((*start)[diff_index] == limit[diff_index])) {
                    diff_index++;
                }

                if (diff_index >= min_length) {
                    // Do not shorten if one string is a prefix of the other
                } else {
                    uint8_t diff_byte = static_cast<uint8_t>((*start)[diff_index]);
                    if (diff_byte < static_cast<uint8_t>(0xff) &&
                        diff_byte + 1 < static_cast<uint8_t>(limit[diff_index])) {
                        (*start)[diff_index]++;
                        start->resize(diff_index + 1);
                        assert(Compare(*start, limit) < 0);
                    }
                }
            }

            void FindShortSuccessor(std::string* key) const override {
                // Find first character that can be incremented
                size_t n = key->size();
                for (size_t i = 0; i < n; i++) {
                    const uint8_t byte = (*key)[i];
                    if (byte != static_cast<uint8_t>(0xff)) {
                        (*key)[i] = byte + 1;
                        key->resize(i + 1);
                        return;
                    }
                }
                // *key is a run of 0xffs.  Leave it alone.
            }
        };
    }  // namespace

    static Comparator* BytewiseComparator() {
        static NoDestructor<BytewiseComparatorImpl> singleton;
        return singleton.get();
    }

}  // namespace  mldb

