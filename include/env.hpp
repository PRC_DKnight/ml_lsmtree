
//Env 是 leveldb 实现用来访问文件系统等操作系统功能的接口。
//调用者可能希望在打开数据库时提供自定义 Env 对象以获得良好的控制；
//例如，对文件系统操作进行速率限制。
//
// 所有 Env 实现对于来自多个线程的并发访问都是安全的，无需任何外部同步。

#pragma once

#include <cstdarg>
#include <cstdint>
#include <string>
#include <vector>

#include "status.hpp"


namespace mldb {

class RandomAccessFile;
class Slice;

class Env {
 public:
  Env();
  Env(const Env&) = delete;
  Env& operator=(const Env&) = delete;

  virtual ~Env();

  // Return a default environment suitable for the current operating
  // system.  Sophisticated users may wish to provide their own Env
  // implementation instead of relying on this default environment.
  //
  // The result of Default() belongs to leveldb and must never be deleted.
  static Env* Default();

  // 创建一个按指定名称顺序读取文件的对象。成功时，将指向新文件的指针存储在结果中并返回 OK。失败时将 nullptr 存储在结果中并返回非 OK。
  // 如果文件不存在，则返回非 OK 状态。当文件不存在时，实现应该返回 NotFound 状态。
  //
  // The returned file will only be accessed by one thread at a time.
  virtual Status NewFile(const std::string& fname,
                         RandomAccessFile* result) = 0;

  virtual bool FileExists(const std::string& fname) = 0;

  virtual Status GetChildren(const std::string& dir,
                             std::vector<std::string>* result) = 0;

  virtual Status RemoveFile(const std::string& fname);

  virtual Status CreateDir(const std::string& dirname) = 0;

  virtual Status RemoveDir(const std::string& dirname);

  virtual Status DeleteDir(const std::string& dirname);

  // Store the size of fname in *file_size.
  virtual Status GetFileSize(const std::string& fname, uint64_t* file_size) = 0;

  // Rename file src to target.
  virtual Status RenameFile(const std::string& src,
                            const std::string& target) = 0;

  // Create and return a log file for storing informational messages.
  //virtual Status NewLogger(const std::string& fname, Logger** result) = 0;

  // Returns the number of micro-seconds since some fixed point in time. Only
  // useful for computing deltas of time.
  virtual uint64_t NowMicros() = 0;

  // Sleep/delay the thread for the prescribed number of micro-seconds.
  virtual void SleepForMicroseconds(int micros) = 0;
};

//// A utility routine: write "data" to the named file.
//Status WriteStringToFile(Env* env, const Slice& data,
//                                        const std::string& fname);
//// A utility routine: read contents of named file into *data
//Status ReadFileToString(Env* env, const std::string& fname,
//                                       std::string* data);

}  // namespace mldb

