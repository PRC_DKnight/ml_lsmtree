﻿//
// Created by Administrator on 2022/1/22 0022.
//

#ifndef MLDB_OPTIONS_H
#define MLDB_OPTIONS_H

#include "include/env.hpp"
#include "filter_policy.hpp"
#include "comparator.hpp"
static int kBlockRestartInterval=16;
namespace mldb{
    enum IndexType{
        LearnedIndex,
        NormalIndex
    };
    struct Options {
            // Create an Options object with default values for all fields.
            Options() : comparator(BytewiseComparator()) {}//, env(Env::Default())
            IndexType index_type_=NormalIndex;
            // Use the specified object to interact with the environment,
            // e.g. to read/write files, schedule background work, etc.
            // Default: Env::Default()
            //Env* env;

            // db 生成的任何内部progresserror 信息如果不为null，则将写入info_log，如果info_log 为null，则将写入与DB 内容存储在同一目录中的文件。
            //Logger* info_log = nullptr;

            // -------------------
            // Parameters that affect performance

            // Amount of data to build up in memory (backed by an unsorted log
            // on disk) before converting to a sorted on-disk file.
            //
            // 较大的值会提高性能，尤其是在批量加载期间。
            // 内存中最多可以同时保存两个写入缓冲区，因此您可能希望调整此参数以控制内存使用情况。
            // 此外，较大的写入缓冲区将导致下次打开数据库时的恢复时间较长。
            size_t write_buffer_size = 4 * 1024 * 1024;

            // 数据库可以使用的打开文件数。如果您的数据库有一个大型工作集（预算每 2MB 工作集一个打开的文件），您可能需要增加此值。
            int max_open_files = 1000;

            // 对块的控制（用户数据存储在一组块中，块是从磁盘读取的单位）。

            // If non-null, use the specified cache for blocks.
            // If null, leveldb will automatically create and use an 8MB internal cache.
            //Cache* block_cache = nullptr;
            const Comparator* comparator;
            // 每个块打包的用户数据的近似大小。请注意，此处指定的块大小对应于未压缩的数据。
            // 如果启用压缩，从磁盘读取的单元的实际大小可能会更小。此参数可以动态更改。
            size_t block_size = 4 * 1024;


            // Leveldb 将在切换到新文件之前将最多此数量的字节写入文件。大多数客户应该不理会这个参数。
            // 但是，如果您的文件系统在处理较大的文件时效率更高，则可以考虑增加该值。
            // 不利的一面是压缩时间更长，因此延迟性能打嗝时间更长。增加此参数的另一个原因可能是您最初填充大型数据库时。
            size_t max_file_size = 2 * 1024 * 1024;

            // 压缩
            //CompressionType compression = kSnappyCompression;

            // If non-null, use the specified filter policy to reduce disk reads.
            // Many applications will benefit from passing the result of
            // NewBloomFilterPolicy() here.
            const FilterPolicy* filter_policy = nullptr;
    };

// Options that control read operations
    struct ReadOptions {
            ReadOptions() = default;

            // If true, all data read from underlying storage will be
            // verified against corresponding checksums.
            bool verify_checksums = false;

            // Should the data read for this iteration be cached in memory?
            // Callers may wish to set this field to false for bulk scans.
            bool fill_cache = true;

            // If "snapshot" is non-null, read as of the supplied snapshot
            // (which must belong to the DB that is being read and which must
            // not have been released).  If "snapshot" is null, use an implicit
            // snapshot of the state at the beginning of this read operation.
            //const Snapshot* snapshot = nullptr;
    };

// Options that control write operations
    struct WriteOptions {
            WriteOptions() = default;

            // If true, the write will be flushed from the operating system
            // buffer cache (by calling WritableFile::Sync()) before the write
            // is considered complete.  If this flag is true, writes will be
            // slower.
            //
            // If this flag is false, and the machine crashes, some recent
            // writes may be lost.  Note that if it is just the process that
            // crashes (i.e., the machine does not reboot), no writes will be
            // lost even if sync==false.
            //
            // In other words, a DB write with sync==false has similar
            // crash semantics as the "write()" system call.  A DB write
            // with sync==true has similar crash semantics to a "write()"
            // system call followed by "fsync()".
            bool sync = false;
    };
}
#endif //MLDB_OPTIONS_H
