
//状态封装了操作的结果。它可能表示成功，也可能表示带有相关错误消息的错误。
// 多个线程可以在没有外部同步的状态下调用 const 方法，但是如果任何线程可以调用非常量方法，则访问同一 Status 的所有线程都必须使用外部同步。

#ifndef MLCPP_STATUS_H_
#define MLCPP_STATUS_H_

#include <algorithm>
#include <string>

#include "slice.hpp"

namespace mldb {
    class Status {
            public:
            enum Code {
                kOk = 0,
                kNotFound = 1,
                kCorruption = 2,
                kNotSupported = 3,
                kInvalidArgument = 4,
                kIOError = 5
            };

            Status():code_num_(kOk){}
            Status& operator=(const Status& rhs){
                code_num_=rhs.code_num_;
                return *this;
            }
            static Status OK(){
                return Status();
            }
            static Status NotFound(){
                return Status(kNotFound,nullptr);
            }

            static Status Corruption(std::string s){
                return Status(kCorruption,s.c_str());
            }
            Status(Code code,const char* msg):code_num_(code),msg(msg){}
        bool ok() const {return code_num_ == kOk;}

    private:

            Code code() const {
                return (code_num_ == 0) ? kOk : code_num_;
            }
            Code code_num_;
            const Slice msg;
    };


}  // namespace mldb

#endif
