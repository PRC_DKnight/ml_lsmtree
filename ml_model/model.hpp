#pragma once

#include <string>
#include <mlpack/core.hpp>
#include <mlpack/methods/linear_regression/linear_regression.hpp>

class MLModel{
public:
    virtual std::string Name()=0;
    virtual void Train()=0;
    virtual void AddData(int key, int index)=0;
    virtual int PredictIndex(int key)=0;
    virtual size_t EncodeTo(char *ptr)=0;
    virtual void DecodeFrom(char* ptr,size_t len)=0;
};

class LinearRegressionModel:public MLModel{
public:
     std::string Name() override{
         return "LinearRegressionModel";
     }
    void inline Train() override {
        lr_model_.Train(key_data_,index_data_);
    }

    void inline AddData(int key, int index) override {
        key_data_<<key<<arma::endr;
        index_data_<<index<<arma::endr;
    }

    int inline PredictIndex(int key) override {
        arma::rowvec prediction;
        arma::mat points({static_cast<double>(key)});
        lr_model_.Predict(points,prediction);
        return prediction[0];
    }
    //序列化为string
    size_t inline EncodeTo(char* ptr) override{
        arma::vec parameters = lr_model_.Parameters();
        for(int i=0;i<parameters.n_rows;i++){
            memcpy(ptr,&parameters.at(i),8);
            ptr+=8;
        }
        return parameters.n_rows*8;
    }

    void inline DecodeFrom(char* ptr,size_t len) override{
        std::vector<double>v ;
        while(len!=0){
            double* a=new double(0);
            memcpy(a,ptr,8);
            len-=8;
            ptr+=8;
            v.push_back(*a);
        }
        lr_model_.parameters=v;
    }
private:
    mlpack::regression::LinearRegression lr_model_;
    arma::mat key_data_;
    arma::rowvec index_data_;
};

