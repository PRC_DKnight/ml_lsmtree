//
// Created by bytedance on 1/17/22.
//
#include <mlpack/core.hpp>
#include "model.hpp"

using namespace mlpack;
int main(){
    LinearRegressionModel lrm;
    lrm.AddData(1,2);
    lrm.AddData(2,3);
    lrm.AddData(3,4);
    lrm.AddData(4,5);
    lrm.Train();
    std::cout<<lrm.PredictIndex(5);
    char* const ptr=new char[100];
    size_t len=lrm.EncodeTo(ptr);
    LinearRegressionModel* lrm1=new LinearRegressionModel();
    //std::cout<<lrm1->PredictIndex(5);
    lrm1->DecodeFrom(ptr,len);
    std::cout<<lrm1->PredictIndex(5);
}

