
#include "include/comparator.hpp"
#include "include/options.h"
#include "memtable/memtable.hpp"
#include "db/dbformat.hpp"
#include "include/slice.hpp"
#include <vector>
#define SIZE 1000000
using namespace mldb;
int main(){
    Options opt;
    InternalKeyComparator ic(opt.comparator);
    MemTable* m=new MemTable(ic);
    std::vector<Slice>* v=new std::vector<Slice>();
    v->reserve(SIZE);
    for(int i=0;i<SIZE;i++){
        std::string a=std::to_string(i);
        Slice sss=(a);
        v->push_back(sss);
    }
    {
        auto start = std::chrono::system_clock::now();
        for(int i=0;i<SIZE;i++){
            m->Add(i,kTypeValue,(*v)[i],(*v)[i]);
        }
        auto end = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        std::cout << SIZE <<" times addition costs: "<< (double(duration.count()) * std::chrono::microseconds::period::num / std::chrono::microseconds::period::den);
        std::cout<<"seconds"<< std::endl;
    }
    {
        Status s;
        std::string val;
        srand(2017);
        auto start = std::chrono::system_clock::now();
        for(int i=0;i<SIZE;i++){
            int spot=rand()%SIZE;
            Slice ss=(*v)[rand()%SIZE];
            LookupKey key(ss,spot);
            m->Get(key,&val,&s);
        }
        auto end = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        std::cout << SIZE <<" times addition costs: "<< (double(duration.count()) * std::chrono::microseconds::period::num / std::chrono::microseconds::period::den);
        std::cout<<"seconds"<< std::endl;
    }

};
