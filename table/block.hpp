//解码 block_builder.cc 生成的块。
//很简单的做法：
//没有重启点，没有压缩，前面是kv对，持久化之前有uint32数组标识kv对的起始offset（这是为了支持继承迭代器的随机访问，可以优化为logn的空间）
#pragma once
#include "block.hpp"
#include "include/status.hpp"
#include "include/comparator.hpp"
#include "include/iterator.h"
#include "include/comparator.hpp"
#include "format.hpp"
#include <cstdint>
#include <cstdio>
#include <vector>
namespace mldb {
    class Block{
    public:
        // Initialize the block with the specified contents.
        explicit Block(const BlockContents& contents);
        Block(const Block&) = delete;

        Block()=default;

        Block& operator=(const Block&) = delete;
        ~Block()=default;
        size_t size() const { return size_; }
        Iterator* NewIterator(const Comparator* comparator=BytewiseComparator());
    private:
        class Iter;
        const char* data_;
        size_t size_=0;
        std::vector<uint32_t>  entry_indexs_;
        uint32_t entry_index_offset=0;//指向稀疏索引的头指针，由data最后一个uint32（entry个数）倒推
        bool owned_;               // Block owns data_[]
    };

    Block::Block(const BlockContents& contents): data_(contents.data.data()),
                                                 size_(contents.data.size()),
                                                 owned_(contents.heap_allocated){
        uint32_t entry_nums=DecodeFixed32(data_+size_-4);
        entry_indexs_.reserve(entry_nums);
        entry_index_offset=size_-entry_nums*4-4;
        uint32_t cur=entry_index_offset;
        while(cur<size_){
            entry_indexs_.push_back(DecodeFixed32(data_+cur));
            cur+=4;
        }
    }
    //返回指向key开头的指针，即刚刚越过key val的两个uint32_t长度标识
    static inline const char* DecodeEntryOffset(const char* p,
                                                uint32_t* key_length,
                                                uint32_t* value_length) {
        *key_length=DecodeFixed32(p);
        *value_length=DecodeFixed32(p+4);
        return p+8;
    }
//迭代器如何前向遍历呢？
//一个entry就是    {|key length|val length|key data|val data|}
    class Block::Iter:public mldb::Iterator{
    private:
        const Comparator* const comparator_;
        const char* const data_;
        Slice key_;
        Slice value_;
        Status status_=Status();
        uint32_t cur_offset_=0;//当前指向的一个entry开始的offset
        uint32_t cur_entry_index_=0;//当前指向第几个entry
        std::vector<uint32_t>*  entry_indexs_;    // Offset of entry_index_ [array] (list of fixed32),to point where the entry data begin in back of the data_
        inline int Compare(const Slice& a, const Slice& b) const {
            return comparator_->Compare(a, b);
        }

        inline const char* NextEntryOffset() const {
            return (value_.data() + value_.size());
        }
        //寻道
        void SeekToEntry(uint32_t index) {
            key_.clear();
            value_.clear();
            cur_entry_index_ = index;
            auto entry_offset_in_data=  (*entry_indexs_)[index];
            uint32_t key_len=0,val_len=0;
            const char* key_offset=DecodeEntryOffset(data_+entry_offset_in_data,&key_len,&val_len);
            key_=Slice(key_offset, key_len);
            value_ = Slice(key_offset + key_len, val_len);
            std::string test=key_.ToString();
            std::string test1=value_.ToString();
            cur_offset_=entry_offset_in_data;
        }

    public:
        Iter(const Comparator* comparator, const char* data, std::vector<uint32_t>*  entry_indexs
        )
                : comparator_(comparator),
                  data_(data),
                  entry_indexs_(entry_indexs){
            SeekToEntry(0);
        }

        bool Valid() const override { return cur_entry_index_<(*entry_indexs_).size(); }

        Slice key() const override {
            assert(Valid());
            return key_;
        }

        Slice value() const override {
            assert(Valid());
            return value_;
        }

        void Next() override {
            assert(Valid());
            cur_entry_index_++;
            const char* next_entry_offset=NextEntryOffset();
            uint32_t key_len,val_len;
            const char* key_offset=DecodeEntryOffset(next_entry_offset,&key_len,&val_len);
            key_=Slice(key_offset,key_len);
            value_=Slice(key_offset+key_len,val_len);
        }
        void Prev() override{
            cur_entry_index_--;
            SeekToEntry(0);
        }
        void Seek(const Slice& target) override {
            uint32_t left = 0;
            uint32_t right = (*entry_indexs_).size()-1;
            int current_key_compare = 0;
            if (Valid()) {
                // 如果我们已经在扫描，则使用当前位置作为起点。如果我们正在寻找的关键是在当前位置之前，这将是有益的。
                current_key_compare = Compare(key_, target);
                if (current_key_compare < 0) {
                    // key_ is smaller than target
                    left = cur_entry_index_;
                } else if (current_key_compare > 0) {
                    right = cur_entry_index_;
                } else {
                    // We're seeking to the key we're already at.
                    return;
                }
            }
            while (left < right) {
                uint32_t mid = (left + right + 1) / 2;
                auto entry_offset_in_data_=  (*entry_indexs_)[mid]  ;
                uint32_t key_len, val_len;
                const char* key_ptr =DecodeEntryOffset(data_ + entry_offset_in_data_,&key_len,&val_len);
                Slice mid_key(key_ptr, key_len);
                if (Compare(mid_key, target) < 0) {
                    // Key at "mid" is smaller than "target".  Therefore all
                    // blocks before "mid" are uninteresting.
                    left = mid;
                } else {
                    // Key at "mid" is >= "target".  Therefore all blocks at or
                    // after "mid" are uninteresting.
                    right = mid - 1;
                }
            }
            assert(current_key_compare == 0 || Valid());
            SeekToEntry(left);

        }

        void SeekToFirst() override {
            SeekToEntry(0);
        }

        void SeekToLast() override {
            SeekToEntry((*entry_indexs_).size() - 1);
        }

        ~Iter(){
            key_.clear();
            value_.clear();
        }
        Status status() const override{
            return Status::OK();
        }
    };

    Iterator* Block::NewIterator(const Comparator* comparator) {
        return new Iter(comparator, data_, &entry_indexs_);
    }


}  // namespace mldb

