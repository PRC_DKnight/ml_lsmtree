//
// Created by Administrator on 2022/1/22 0022.
//
#pragma once
#include "table/format.hpp"
#include "include/slice.hpp"
#include "include/options.h"
#include "ml_model/model.hpp"
#include <string>

namespace mldb {
    class BlockBuilder {
    public:
        explicit BlockBuilder(const Options* options):options_(options){
        }
        BlockBuilder(const BlockBuilder&) = delete;
        BlockBuilder& operator=(const BlockBuilder&) = delete;
        // 重置内容，就好像刚刚构建了 BlockBuilder。
        void Reset();

        // REQUIRES: 自上次调用 Reset() 后，未调用 Finish()。
        // REQUIRES: 键大于任何先前添加的键  调用Add前必须保证有序
        void Add(const Slice& key, const Slice& value);
        // 完成构建块并返回一个引用块内容的Slice。返回的Slice将在此构建器的生命周期内或调用 Reset() 之前保持有效。
        Slice Finish();
        // Return true iff no entries have been added since the last Reset()
        bool empty() const { return buffer_.empty(); }
        size_t Size() const;
    private:
        std::string buffer_;// Destination buffer
        bool finished_= false;                   // Has Finish() been called?
        std::string last_key_;
        std::vector<uint32_t> entry_offsets_;
        const Options* options_;
    };

    void BlockBuilder::Reset() {
        buffer_.clear();
        finished_ = false;
        last_key_.clear();
        entry_offsets_.clear();
        entry_offsets_.shrink_to_fit();
    }

    size_t BlockBuilder::Size() const {
        return (entry_offsets_.size());
    }

    Slice BlockBuilder::Finish() {
        //将entry索引数组加到block尾巴
        for (size_t i = 0; i < entry_offsets_.size(); i++) {
            PutFixed32(&buffer_, entry_offsets_[i]);
            uint32_t test= DecodeFixed32(buffer_.c_str()+buffer_.size()-4);
            std::cout<<test;
        }
        PutFixed32(&buffer_, entry_offsets_.size());
        uint32_t test= DecodeFixed32(buffer_.c_str()+buffer_.size()-4);
        std::cout<<test;
        finished_ = true;
        //TODO:如果是拷贝构造，无法在block恢复索引数？
        return Slice(std::move(buffer_));
    }
//必须在上层保证有序！！！
    void BlockBuilder::Add(const Slice& key, const Slice& value) {
        entry_offsets_.emplace_back(buffer_.size());
        Slice last_key_piece(last_key_);
        PutFixed32(&buffer_, key.size());
        PutFixed32(&buffer_, value.size());
        buffer_.append(key.data()  ,key.size());
        buffer_.append(value.data(), value.size());
        last_key_=key.ToString();
    }

}  // namespace mldb

