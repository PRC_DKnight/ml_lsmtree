#ifndef MLDB_FORMAT_H_
#define MLDB_FORMAT_H_

#include <cstdint>
#include <string>
#include "util/fileio.h"
#include "include/slice.hpp"
#include "include/status.hpp"
#include "util/coding.hpp"

namespace mldb {

    struct BlockContents {
        Slice data;           // Actual contents of data
        bool cachable;        // True iff data can be cached
        bool heap_allocated;  // True iff caller should delete[] data.data()
    };
class Block;


// BlockHandle 是指向存储数据的文件范围的指针
// block or a meta block.
class BlockHandle {
 public:
  // Maximum encoding length of a BlockHandle 为啥是20
  enum {  kMaxEncodedLength = 10 + 10 };
    BlockHandle(uint64_t offset,size_t size)
            : offset_(offset), size_(size) {}
    BlockHandle()
            : offset_(~static_cast<uint64_t>(0)), size_(~static_cast<uint64_t>(0)) {}

  // The offset of the block in the file.
  uint64_t offset() const { return offset_; }
  void set_offset(uint64_t offset) { offset_ = offset; }

  // The size of the stored block
  uint64_t size() const { return size_; }
  void set_size(uint64_t size) { size_ = size; }

  void EncodeTo(std::string* dst) const{
      // Sanity check that all fields have been set
      assert(offset_ != ~static_cast<uint64_t>(0));
      assert(size_ != ~static_cast<uint64_t>(0));
      PutVarint64(dst, offset_);
      PutVarint64(dst, size_);
    }
  Status DecodeFrom(Slice* input){
      if (GetVarint64(input, &offset_) && GetVarint64(input, &size_)) {
          return Status();
      } else {
          return Status(Status::kOk,"bad block handle");
      }
  }
 private:
  uint64_t offset_;
  uint64_t size_;
};


static const uint64_t kTableMagicNumber = 0xdb4775000b99fb57ull;


 //Footer封装了存储在SST尾部的固定信息
class Footer {
public:
    // Encoded length of a Footer.  Note that the serialization of a
    // Footer will always occupy exactly this many bytes.  It consists
    // of two block handles and a magic number.为啥是48
    enum { kEncodedLength = 2 * BlockHandle::kMaxEncodedLength + 8 };

    Footer() = default;

    // The block handle for the metaindex block of the table
    const BlockHandle& metaindex_handle() const { return metaindex_handle_; }
    void set_metaindex_handle(const BlockHandle& h) { metaindex_handle_ = h; }

    // The block handle for the index block of the table
    const BlockHandle& index_handle() const { return index_handle_; }
    void set_index_handle(const BlockHandle& h) { index_handle_ = h; }

    void EncodeTo(std::string* dst) const{
        const size_t original_size = dst->size();
        metaindex_handle_.EncodeTo(dst);
        index_handle_.EncodeTo(dst);
        dst->resize(2 * BlockHandle::kMaxEncodedLength);  // Padding
        PutFixed32(dst, static_cast<uint32_t>(kTableMagicNumber & 0xffffffffu));
        PutFixed32(dst, static_cast<uint32_t>(kTableMagicNumber >> 32));
        assert(dst->size() == original_size + kEncodedLength);
        (void)original_size;  // Disable unused variable warning.
    }
    Status DecodeFrom(Slice* input){
        const char* magic_ptr = input->data() + kEncodedLength - 8;
        const uint32_t magic_lo = DecodeFixed32(magic_ptr);
        const uint32_t magic_hi = DecodeFixed32(magic_ptr + 4);
        const uint64_t magic = ((static_cast<uint64_t>(magic_hi) << 32) |
                                (static_cast<uint64_t>(magic_lo)));
        if (magic != kTableMagicNumber) {
            return Status(Status::kNotSupported,"not an sstable (bad magic number)");
        }

        Status result = metaindex_handle_.DecodeFrom(input);
        if (result.ok()) {
            result = index_handle_.DecodeFrom(input);
        }
        if (result.ok()) {
            // We skip over any leftover data (just padding for now) in "input"
            const char* end = magic_ptr + 8;
            *input = Slice(end, input->data() + input->size() - end);
        }
        return result;
    }

private:
    BlockHandle metaindex_handle_;
    BlockHandle index_handle_;
    //BlockHandle mlmodel_handle_;

};



}  // namespace leveldb

#endif  // STORAGE_LEVELDB_TABLE_FORMAT_H_
