﻿//
// Created by Administrator on 2022/1/31 0031.
//

#ifndef MLDB_TABLE_H
#define MLDB_TABLE_H
#include "ml_model/model.hpp"
#include "block.hpp"
#include "format.hpp"
#include "include/options.h"
#include "filter_block.hpp"
#endif //MLDB_TABLE_H
namespace mldb{
    static Status ReadBlock(FileReader* file,const BlockHandle& handle,BlockContents* result){
        result->data = Slice();
        result->cachable = false;
        result->heap_allocated = false;
        size_t handle_size = static_cast<size_t>(handle.size());
        Slice contents(handle_size);
        Status s = file->Read(handle.offset(), &contents);
        if (!s.ok()) {
            return s;
        }
        if (contents.size() != handle_size) {
            return Status::Corruption("truncated block read");
        }
        return Status();
    }

class Table{
    struct TableMeta {
        ~TableMeta() {
            delete filter;
            delete[] filter_data;
            delete index_block;
            //delete ml_block;
        }
//        uint64_t cache_id;  暂时不做上层缓存
        BlockHandle metaindex_handle_;  // Handle to metaindex_block: saved from footer
        Block* index_block=nullptr;
        //Block* ml_block=nullptr;
        FilterBlockReader* filter;
        const char* filter_data;
        FileReader* file;
        Options options;
    };

    //load meta  TODO:
    Status Open(FileReader* file,uint64_t size, Table** table){
        if (size < Footer::kEncodedLength) {
            return Status(Status::kCorruption,"file is too short to be an sstable");
        }
        Slice footer_input(Footer::kEncodedLength);
        meta_->file->SeekEnd();
        Status s=meta_->file->Read(size - Footer::kEncodedLength,&footer_input);
        if (!s.ok()) return s;

        Footer footer;
        s = footer.DecodeFrom(&footer_input);
        if (!s.ok()) return s;
        // Read the index block
        BlockContents index_block_contents;

        s = ReadBlock(file,footer.index_handle(), &index_block_contents);
        return s;
    }
//    Status OpenTable(std::string filepath,MLModel* ml_){
//        file_reader_=
//        *table = nullptr;
//        file_reader_
//        if (size < Footer::kEncodedLength) {
//            return Status::Corruption("file is too short to be an sstable");
//        }
//        char footer_space[Footer::kEncodedLength];
//        Slice footer_input;
//        Status s = file->Read(size - Footer::kEncodedLength, Footer::kEncodedLength,
//                              &footer_input, footer_space);
//        file_reader_
//    }

Table(const Table&)=delete;
Table& operator=(const Table &)=delete;

private:
    TableMeta* const meta_;
    FileReader file_reader_;
    void ReadMeta(const Footer& footer){

    }
    void ReadFilter(){

    }
};
}