#pragma once
#include "util/fileio.h"
#include "table/block_builder.hpp"
#include "filter_block.hpp"
#include "ml_model/model.hpp"
#include "include/options.h"
#include<cstring>
#include <memory>
#ifndef MLDB_TABLE_BUILDER_H_
#define MLDB_TABLE_BUILDER_H_


namespace mldb {
static size_t kBlock_Length=2<<20;//2mb
class TableBuilder {
 public:
  TableBuilder(std::string filepath,const Options& opt):opt_(opt),w_(new FileWriter(filepath)),data_block_(&opt_), index_block_(&opt_){
      if(opt.index_type_==LearnedIndex){
          ml_= std::make_unique<LinearRegressionModel>();
      }
      if (filter_block != nullptr) {
          filter_block->StartBlock(0);
      }
  }
  TableBuilder(const TableBuilder&) = delete;
  TableBuilder& operator=(const TableBuilder&) = delete;
  // REQUIRES: Either Finish() or Abandon() has been called.
  ~TableBuilder(){
  }
    void Add(const Slice& key, const Slice& value){
        assert(!closed_);
        if (num_entries_ > 0) {
            assert(opt_.comparator->Compare(key, Slice(last_key_))>0);
        }
        if (!ok()) return;

        if (add_index_entry_&&opt_.index_type_==NormalIndex) {
            std::string handle_encoding;
            cur_handle_.EncodeTo(&handle_encoding);
            index_block_.Add(last_key_, Slice(handle_encoding));
            add_index_entry_ = false;
        }
        if (filter_block != nullptr) {
            filter_block->AddKey(key);
        }
        last_key_.assign(key.data(), key.size());

        if(opt_.index_type_==LearnedIndex){//如果是学习索引，加入训练集
            ml_->AddData(std::atoi(key.data()),std::atoi(value.data()));
        }
        num_entries_++;
        data_block_.Add(key, value);
        if (data_block_.Size() >= opt_.block_size) {
            Flush();
        }
    }
    void Flush(){
        if (!ok()) return;
        if (data_block_.empty()) return;
        if(WriteBlock(&data_block_,&cur_handle_).ok()==ok()){
            add_index_entry_ = true;
        }

    }

    Status Finish(){
        Flush();
        closed_ = true;
        BlockHandle filter_block_handle, metaindex_block_handle, index_block_handle;
        // Write filter block
        if (ok() && filter_block != nullptr) {
            WriteRawBlock(filter_block->Finish(),&filter_block_handle);
        }
        // Write metaindex block  指示filter的index
        if (ok()) {
            BlockBuilder meta_index_block(&opt_);
            if (filter_block != nullptr) {
                // Add mapping from "filter.Name" to location of filter data
                std::string key = "filter.";
                key.append(opt_.filter_policy->Name());
                std::string handle_encoding;
                filter_block_handle.EncodeTo(&handle_encoding);
                meta_index_block.Add(key, handle_encoding);
            }
            // TODO(postrelease): Add stats and other meta blocks
            WriteBlock(&meta_index_block, &metaindex_block_handle);
        }

        // Write index block
        if (ok()&&opt_.index_type_==NormalIndex) {
            WriteBlock(&index_block_,&index_block_handle);
        }else if (ok()&&LearnedIndex==opt_.index_type_) {
            char* ml_data=new char[16];
            ml_->EncodeTo(ml_data);
            index_block_.Add(ml_->Name(),ml_data);
            WriteBlock(&index_block_,&index_block_handle);
        }
        if (ok()) {
            Footer footer;
            footer.set_metaindex_handle(metaindex_block_handle);
            footer.set_index_handle(index_block_handle);
            std::string footer_encoding;
            footer.EncodeTo(&footer_encoding);
            w_->WriteSequence(footer_encoding);
            if (status_.ok()) {
                last_block_start_ += footer_encoding.size();
            }
        }
        return status_;

    }
private:

  // Return non-ok iff some error has been detected.
  Status status() const{
      return status_;
  }


  // Number of calls to Add() so far.
  uint64_t NumEntries() const{
      return num_entries_;
  }

  uint64_t FileSize() const{
      return last_block_start_;
  }




 private:
  bool ok() const { return status_.ok(); }
    // File format contains a sequence of blocks where each block has:
    //    block_data: uint8[n]
    //    type: uint8

    Status status_;
    BlockBuilder data_block_;
    BlockBuilder index_block_;
    std::string last_key_;
    int64_t num_entries_=0;
    uint64_t last_block_start_=0;
    bool closed_=false;  // Either Finish() or Abandon() has been called.
    std::unique_ptr<FileWriter> w_;
    std::unique_ptr<MLModel> ml_;
    std::unique_ptr<FilterBlockBuilder> filter_block;
    //优化:就是说做稀疏索引的key不一定是key的全部，可能拿一个字母就行了，这个字母大于前面所有的键，小于后面所有的键
    bool add_index_entry_=false;//这一次add应该加入index

    BlockHandle cur_handle_;
    Options opt_;

    //没有压缩算法，没有crc，就直接写
    Status WriteBlock(BlockBuilder* block,BlockHandle* handle){
        assert(ok());
        WriteRawBlock(block->Finish(),handle);
        data_block_.Reset();
        return Status();
    }
    void WriteRawBlock(const Slice& block_contents, BlockHandle* handle) {
        w_->WriteSequence(block_contents);
        handle->set_offset(last_block_start_);
        handle->set_size(block_contents.size());
        last_block_start_+=block_contents.size();
    }


};







}  // namespace mldb

#endif
