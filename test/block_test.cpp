#include "table/table_builder.h"
#include "table/block.hpp"
#include <gtest/gtest.h>
#include "table/format.hpp"
#include "include/options.h"
#include "include/comparator.hpp"
using namespace mldb;

class TableTest : public testing::Test {
public:
    TableTest(){

    }

protected:

};
//
TEST_F(TableTest, BlockBuilder){
    Options opt;
    BlockBuilder bb(&opt);
    std::vector<std::pair<std::string,std::string>>v;
    for(int i=0;i<100;i++){
        v.push_back({std::to_string(i),std::to_string(i*i)});
    }
    sort(v.begin(),v.end());
    for(int i=0;i<100;i++){
        bb.Add(v[i].first,v[i].second);
    }
    Slice block_content=bb.Finish();
    BlockContents bc{block_content, false, false};
    Block block(bc);
    auto iter=block.NewIterator();
    for(int i=0;i<100;i++){
        EXPECT_EQ(std::string(iter->key().data(),iter->key().size()),v[i].first);
        EXPECT_EQ(std::string(iter->value().data(),iter->value().size()),v[i].second);
        iter->Next();
    }
}
TEST_F(TableTest, TableBuilder){
    Options opt;
    TableBuilder tb(GetSSTPath(1),opt);
    std::vector<std::pair<std::string,std::string>>v;
    for(int i=0;i<100;i++){
        v.push_back({std::to_string(i),std::to_string(i*i)});
    }
    sort(v.begin(),v.end());
    for(int i=0;i<100;i++){
        tb.Add(v[i].first,v[i].second);
    }
    tb.Finish();

}

