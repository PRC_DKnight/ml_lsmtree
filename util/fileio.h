﻿//
// Created by Administrator on 2022/1/25 0025.
//
#include <fstream>
#include "include/slice.hpp"
#include "include/status.hpp"
#ifndef MLDB_FILEIO_H
#define MLDB_FILEIO_H
namespace mldb{
    std::string static GetSSTPath(uint32_t id){
        return "D:\\APPS\\CPPProject\\ML_LSMTree\\util\\test\\"+std::to_string(id)+".mldb";
    }

    class FileWriter{
    public:
        FileWriter(std::string file_name){
            out_stream.open(file_name);
        }
        ~FileWriter(){
            out_stream.close();
        }
        void WriteSequence (const std::string& data){
            out_stream.write(data.data(),data.size());
        }
        void WriteSequence (const Slice& data){
            out_stream.write(data.data(),data.size());
        }
        void Close(){
            out_stream.close();
        };
        void Write(uint64_t offset,Slice& data){

        }
    private:
        std::ofstream out_stream;
    };

    class FileReader{
    public:
        FileReader(std::string file_name){
            is_.open(file_name);
        }
        ~FileReader(){
            is_.close();
        }
        void SeekEnd(){
            is_.seekg(0,std::ios::end);
        }
        void ReadSequence(char* const data,size_t size){
            is_.get(data,size);
        }
        void Sync(){
            is_.sync();
        }

        Status Read(uint64_t offset,size_t size,char* const data){
            SeekTo(offset);
            is_.get(data,size);
            return Status();
        }

        Status Read(uint64_t offset,Slice* data){
            SeekTo(offset);
            is_.get(const_cast<char *>(data->data()),data->size());
            return Status();
        }

        void SeekTo(uint64_t offset){
            if(offset==offset_)return;
            is_.seekg(offset,std::ios::beg);
            offset_=offset;
        }
    private:
        uint64_t offset_;
        std::ifstream is_;
    };

}

#endif //MLDB_FILEIO_H
